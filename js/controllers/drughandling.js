/**
 * Created by Sachin on 7/2/2017.
 */
/**
 * Created by Sachin on 5/10/2017.
 */
angular
    .module('app')
    .controller('view_drugs', view_drugs);

view_drugs.$inject = ['$http', '$scope', 'urlService'];

function view_drugs($http, $scope, urlService) {

    console.log(urlService.baseUrl);

    $scope.AddDrug = function () {

        console.log($scope.category.name);
        $scope.durginfoARRAY = {
            category:$scope.category._id,
            type:$scope.type._id,
            name:$scope.drugname,
            price:$scope.price,
            reorderLevel:$scope.reorderlevel,
            dangerLevel:$scope.dangerlevel,
            Remarks:$scope.Remarks,
            quantity:$scope.quantity,
        };
        console.log($scope.durginfoARRAY);
        $http.post(`${urlService.baseUrl}drugs/addDrug`, $scope.durginfoARRAY);
        $scope.durginfo.push($scope.durginfoARRAY);
    };



    this.$http = $http;
    var vm = this;

    $http.get(`${urlService.baseUrl}drugs/getAll`).then(function (result) {
        vm.durginfoData = result.data;

        $scope.durginfo = [{
            Category:"",
            Type:"",
            Name:"",
            Price:"",
            Reorderlevel:"",
            Dangerlevel:"",
            Remarks:""
        }];

        $scope.durginfo = vm.durginfoData;
        console.log($scope.durginfo);
    });

    $scope.deleteDrug = function (val) {
        console.log("sachin");
        console.log(val);
        $http.delete(`${urlService.baseUrl}drugs/` + val._id, val).then(function (result) {
            console.log(result);
            $scope.durginfo.slice();
            console.log($scope.drugs);
            $scope.getData();
        })
    }

    $scope.saveUpdateDrug = function () {
        console.log("sahcin");
        $scope.dupdate = {
            category:$scope.category._id,
            type:$scope.type._id,
            name:$scope.drugname,
            price:$scope.price,
            reorderLevel:$scope.reorderlevel,
            dangerLevel:$scope.dangerlevel,
            Remarks:$scope.Remarks,
            quantity:$scope.quantity,
        };
        console.log($scope.dupdate);
        $http.put('http://localhost:5000/drugs/' + $scope.id, $scope.dupdate).then(function (result) {
            console.log(result);
            $scope.getData();
            $scope.clear();
            $scope.showBtn = false;
        })
    }


    $scope.contentVisibility = false;

    $scope.populateContentdrugs = () => {
        $scope.contentVisibility = true;
        $http.get(`${urlService.baseUrl}drugs/getAll`).then(result => {
            vm.drugContentData = result.data;
            $scope.drugContents = vm.drugContentData;
            console.log("ta hukanna");
            console.log($scope.drugContents);

        }).catch(err => {
            throw err;
        });
    };

    $scope.getCategory = () => {
        $http.get(`${urlService.baseUrl}drugs/categories/getAll`).then(result => {
            vm.drugCategoryData = result.data;
            $scope.drugCategories = vm.drugCategoryData;
            console.log($scope.drugCategories);
        }).catch(err => {
            throw err;
        });
    };

    $scope.getCategory();

    $scope.getDrugType = () => {
        $http.get(`${urlService.baseUrl}drugs/types/getAll`).then(result => {
            vm.drugDrugTypeData = result.data;
            $scope.drugDrugTypes = vm.drugDrugTypeData;
            console.log($scope.drugDrugTypes);
        }).catch(err => {
            throw err;
        });
    };

    $scope.getDrugType();
}
