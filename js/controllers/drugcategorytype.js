/**
 * Created by Sachin on 5/11/2017.
 */
angular
    .module('app')
    .controller('add_categories', add_categories)
    .controller('view_drugcategory', view_drugcategory)
    .controller('add_drugtypes', add_drugtypes)
    .controller('view_drugtype', view_drugtype)
    .controller('reload', reload);

view_drugcategory.$inject = ['$http', '$scope','urlService'];
view_drugtype.$inject = ['$http', '$scope','urlService'];

function add_categories($scope, $http, urlService){
    $scope.AddCategorie = function () {
        console.log($scope.Categorie);
        $http.post(`${urlService.baseUrl}drugs/categories/add`, $scope.Categorie);
    };
}

function view_drugcategory($http, $scope, urlService) {

    this.$http = $http;
    var vm = this;

    $http.get(`${urlService.baseUrl}drugs/categories/getAll`).then(function (result) {
        vm.durgcategoryinfoData = result.data;

        $scope.durgcategoryinfo = [{
            Name:""
        }];

        $scope.durgcategoryinfo = vm.durgcategoryinfoData;
        console.log($scope.durgcategoryinfo);
    });
}

function add_drugtypes($scope, $http){
    $scope.AddDrugtype = function () {
        console.log($scope.drugarray);
        $http.post(`${urlService.baseUrl}drugs/types/add`,$scope.drugarray);
    };
}

function view_drugtype($http, $scope, urlService) {

    this.$http = $http;
    var vm = this;

    $http.get(`${urlService.baseUrl}drugs/types/getAll`).then(function (result) {
        vm.durgtypeinfoData = result.data;

        $scope.durgtypeinfo = [{
            Name:""
        }];

        $scope.durgtypeinfo = vm.durgtypeinfoData;
        console.log($scope.durgtypeinfo);
    });
}
